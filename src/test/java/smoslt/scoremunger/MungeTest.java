package smoslt.scoremunger;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreTracker;
import smoslt.given.ScoreTrackerImpl;

public class MungeTest {
	/*
	 * Assumes that ScoreTracker would always maintain Fail, Hours, then at
	 * least 4 other ScoreKeys
	 */
	@Test
	public void testMunge() {
		Munge munge = new Munge();
		ScoreTracker scoreTracker = new ScoreTrackerImpl(null);
		int i = 0;
		List<ScoreMunger> scoreMungers = new ArrayList<ScoreMunger>();
		for (ScoreKey scoreKey : scoreTracker.getHeaders()) {
			if (i == 0) {
				scoreMungers.add(new ScoreMunger("" + scoreKey, 100, 0));
			} else if (i == 1) {
				scoreMungers.add(new ScoreMunger("" + scoreKey, Integer.MAX_VALUE, 0));
			} else if (i == 2) {
				scoreMungers.add(new ScoreMunger("" + scoreKey, 140, 90));
			} else if (i == 3) {
				scoreMungers.add(new ScoreMunger("" + scoreKey, 130, 60));
			} else if (i == 4) {
				scoreMungers.add(new ScoreMunger("" + scoreKey, 102, 60));
			} else {
				scoreMungers.add(new ScoreMunger("" + scoreKey, 100, 60));
			}
			i++;
		}
		munge.scoreMungers = scoreMungers;
		munge.munge();
		int testValue = 0;
		for (int j = 0; j < munge.scoreWeights.size(); j++) {
			testValue = testValue + munge.scoreWeights.get(j);
		}
		assertTrue(testValue < munge.totalRequiredScoreWeight() + 2
				&& testValue > munge.totalRequiredScoreWeight() - 2);
		assertEquals(140, (int) munge.scoreWeights.get(0));
		assertEquals(130, (int) munge.scoreWeights.get(1));
		assertEquals(102, (int) munge.scoreWeights.get(2));
		assertTrue(munge.scoreWeights.get(3) < 100);
	}
}
