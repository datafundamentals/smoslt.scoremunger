package smoslt.scoremunger;

import static org.junit.Assert.*;

import org.junit.Test;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;

public class GetScoreMungersTest {

	@Test
	public void testParseMungeLine() {
		String line = ""+ScoreKey.RDD + ",100,50";
		ScoreMunger scoreMunger = new GetScoreMungersImpl().parseMungeLine(line);
		assertEquals(100, scoreMunger.getScoreWeight());
		assertEquals(50, scoreMunger.getScoreLimit());
		assertEquals(ScoreKey.RDD, scoreMunger.getScoreKey());
	}

}
