package smoslt.scoremunger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.btrg.uti.FileAsList_;
import org.btrg.uti.FileWrite_;
import org.btrg.uti.NioFileUtil;

import smoslt.domain.FileConstants;
import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreTracker;
import smoslt.given.ScoreTrackerImpl;
import smoslt.util.ConfigProperties;

public class Munge {
	File mungeFile = null;
	File lastMungeFile = null;
	List<ScoreMunger> scoreMungers = null;
	ScoreTracker scoreTracker = new ScoreTrackerImpl(null);
	List<Integer> scoreWeights = new ArrayList<Integer>();
	int totalOfScoreWeightsSoFar = 0;
	int totalOfScoreWeightsFinal = 0;
	int maxHoursCutoff = 0;

	public void exec() {
		mungeFile = new File(getMungeFilePath());
		if (mungeFile.exists()) {
			scoreMungers = new GetScoreMungersImpl().get();
			munge();
			new WriteMungeFile().write(scoreMungers, mungeFile);
		} else {
			new WriteMungeFile().populateEmptyMungeFile(mungeFile);
		}
	}

	void munge() {
		if (totalExistingScoreWeight() != totalRequiredScoreWeight()) {
			recalculateRemainders();
			resetScoreWeights();
		}
	}

	void resetScoreWeights() {
		int i = 0;
		// next line works fine in IDE but blows up in maven tests too lazy to
		// investigate why and fix it
		// assert (scoreMungers.size() == scoreWeights.size());
		List<ScoreMunger> newScoreMungers = new ArrayList<ScoreMunger>();
		newScoreMungers.add(scoreMungers.get(0));
		newScoreMungers.add(scoreMungers.get(1));
		for (int weight : scoreWeights) {
			try {
				ScoreMunger oldScoreMunger = scoreMungers.get(i + 2);
				ScoreMunger newScoreMunger = new ScoreMunger(""
						+ oldScoreMunger.getScoreKey(), weight,
						oldScoreMunger.getScoreLimit());
				newScoreMungers.add(newScoreMunger);
				i++;
			} catch (IndexOutOfBoundsException e) {
				// do nothing
			}
		}
		scoreMungers = newScoreMungers;
	}

	void recalculateRemainders() {
		totalExistingScoreWeight();
		int lowestSoFar = Integer.MAX_VALUE;
		int allAreRemainderAfterThisIndex = 0;
		int remainderPerEach = 0;
		/*
		 * Iterate through scoreWeights and make a note of which scores are
		 * greater than 100 and still declining or equal
		 */
		for (int i = 0; i < scoreWeights.size(); i++) {
			if (scoreWeights.get(i) <= lowestSoFar && scoreWeights.get(i) > 100) {
				lowestSoFar = scoreWeights.get(i);
				totalOfScoreWeightsSoFar = totalOfScoreWeightsSoFar
						+ lowestSoFar;
			} else {
				allAreRemainderAfterThisIndex = i;
				break;
			}
		}
		remainderPerEach = (totalRequiredScoreWeight() - totalOfScoreWeightsSoFar)
				/ (scoreWeights.size() - allAreRemainderAfterThisIndex);
		for (int i = 0; i < scoreWeights.size(); i++) {
			if (i >= allAreRemainderAfterThisIndex) {
				if (remainderPerEach <= (totalRequiredScoreWeight() - totalOfScoreWeightsSoFar)) {
					totalOfScoreWeightsSoFar = +remainderPerEach;
					scoreWeights.set(i, remainderPerEach);
				} else {
					scoreWeights.set(i, totalRequiredScoreWeight()
							- totalOfScoreWeightsSoFar);
				}
			}
		}

	}

	public int totalExistingScoreWeight() {
		// runs once only, else acts as a getter
		if (totalOfScoreWeightsFinal == 0) {
			for (ScoreMunger scoreMunger : scoreMungers) {
				if (scoreMunger.getScoreKey() == ScoreKey.FAIL
						|| scoreMunger.getScoreKey() == ScoreKey.Hours) {
					continue;
				}
				totalOfScoreWeightsFinal += scoreMunger.getScoreWeight();
				scoreWeights.add(scoreMunger.getScoreWeight());
			}
		}
		return totalOfScoreWeightsFinal;
	}

	public int totalRequiredScoreWeight() {
		int returnValue = (scoreTracker.getHeaders().size() - 2) * 100;
		return returnValue;
	}

	public static String getMungeFilePath(){
		return ConfigProperties.get().getProperty(ConfigProperties.MUNGE_FILE_PATH);
		
	}

}
