package smoslt.scoremunger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.btrg.uti.FileWrite_;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;
import smoslt.given.ScoreTrackerImpl;

public class WriteMungeFile {

	void write(List<ScoreMunger> scoreMungers, File mungeFile) {
		StringBuffer sb = new StringBuffer();
		for (ScoreMunger scoreMunger : scoreMungers) {
			sb.append("" + scoreMunger.getScoreKey() + ","
					+ scoreMunger.getScoreWeight() + ","
					+ scoreMunger.getScoreLimit() + "\n");
		}
		new FileWrite_(mungeFile.getAbsolutePath(), sb.toString());
	}

	void populateEmptyMungeFile(File mungeFile) {
		List<ScoreMunger> scoreMungers = new ArrayList<ScoreMunger>();
		ScoreMunger scoreMunger = null;
		for (ScoreKey scoreKey : new ScoreTrackerImpl(null).getHeaders()) {
			if (scoreKey != ScoreKey.Hours) {
				scoreMunger = new ScoreMunger("" + scoreKey, 100, 0);
			} else {
				scoreMunger = new ScoreMunger("" + scoreKey, 0,
						Integer.MAX_VALUE);
			}
			scoreMungers.add(scoreMunger);
		}
		write(scoreMungers, mungeFile);
	}
}
